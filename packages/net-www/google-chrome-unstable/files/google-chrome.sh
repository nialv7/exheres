#!/bin/bash

# Allow users to override command-line options
if [[ -f ~/.config/chrome@CHANNEL@-flags.conf ]]; then
   CHROME_USER_FLAGS="$(cat ~/.config/chrome@CHANNEL@-flags.conf)"
fi

# Launch
exec /opt/google/chrome@CHANNEL@/google-chrome@CHANNEL@ $CHROME_USER_FLAGS "$@"
